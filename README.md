# Specsheet Generator 

This project originated with a Handlebars Template and a nodeJs utility to make some pdf documents real quick like.

The next step was to create a Svelte UI that utilized firebase to allow creation of the JSON files that fed the pdf generator. 

Now here we are with a Springboot service and Svelte app to to the same thing. But this time we are generating the PDF in java using Handlebars and the IText Pdf library. 

***This is something that was thrown together quickly, as a proof of concept, keep that in mind before using it as an example!***  


## Project Structure 

This single repo contains all the pieces to run the project, If you look at it for too long it should make sense. 

Anything in the root folder pertains to the entire project, therefore the docker-compose file found here is used to run the whole thing. The docker-compose file found in the API folder, is used for API development and so on. 

The UI folder contains the Svelte UI code and how to use it. 

## Purpose  
This project is used for practicing certain techniques in Springboot and Svelte, while being a little bit more involved than your typical TODO application. (I just can't do any more TODO apps.)


## How to Use 

Install docker or a docker equivalent and in this folder run the following. 

```
docker-compose up -d
```

After a few minutes navigate to [FTC Specsheet Generator](http://localhost:8081).

### Data Initialization
In the example-data folder there is an example JSON object for a boat. Use a client like PostMan to post this object to http://localhost:8080/boats. 

From there refresh the UI and change the image to one of the included or one of your own. From there you can exercise the UI, and modify the values.  This makes it easy to pre-populate a boat with some basic information so you can see how the PDF preview and generation works.

## Screenshots  

![UI](./screenshots/Full-UI.png)  
The UI showing the editor on the right and the PDF preview on the left.


![UI Expanded](./screenshots/Full-UI-expanded.png)  
The UI showing the editor, preview, and the expanded column component.


