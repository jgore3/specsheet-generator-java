import { writable } from "svelte/store";
export let store;
export const initialValue = ()=> {
    return {
        boatData : {}
    }
}

export const makeBoatStorage = (args)=>{
    let initial = initialValue();
    store = writable(initial, makeSubscribe(initial, args));
    return store;
}

const unsubscribe = ()=>{

}

const makeSubscribe = (data, _args)=>{
    return set =>{
        fetchBoatData(data, _args);
        return unsubscribe;
    }
}

export const update = async (_args) =>{
    let unsubscribe;
    try{
        unsubscribe = store.subscribe(async ($value)=>{
            const response = await fetch(`http://localhost:8080/boats/${_args.id}`, {
                method : 'PUT',
                body:JSON.stringify($value),
                headers:{
                    "Content-Type":"Application/Json"
                }
            });
            if(unsubscribe){
                unsubscribe();
            }
            if(response.ok){
                const boat = await response.json();
                store.set(boat);
            }else{
                const errorText = response.text();
                throw new Error(errorText);
            }
        })
       
    }catch(error){
      
    }finally{
        if(unsubscribe){
            unsubscribe();
        }
    }
}


export const fetchBoatData = async (data, _args) =>{

    try{
        const response = await fetch(`http://localhost:8080/boats/${_args?.id}`);

        if(response.ok){
            const boat = await response.json();
            store.set(boat);
        }else{
            const errorText = response.text();
            throw new Error(errorText);
        }
    }catch(error){
        data.error = error;
        store.set(data);
    }
}

