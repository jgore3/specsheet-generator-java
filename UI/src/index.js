import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App/App.svelte';

let app = new App({
    target: document.body,
});

export default app;