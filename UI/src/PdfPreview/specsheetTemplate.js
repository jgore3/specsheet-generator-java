const specSheetTemplate = `
<!DOCTYPE html>
<html>


<style>
    .page {
        page-break-after: always;
        position: relative;
        width: 8.5in;
        height: 1090px;
        font-size: medium;
    }

    .page-content {
        position: absolute;
        width: 8.125in;
        height: 10.625in;
        left: 0.1875in;
        top: 0.1875in;
    }
    
    .outer-border {
        height: 99.9%;
        min-height: 100%;
        width: 99.9%;
        box-sizing: border-box;
        overflow-x: hidden;
        overflow-y: hidden;
        border: solid 5px black;
        border-radius: 30px;
        padding: 5px;
    }
    
    .inner-border {
        border: solid 5px #5a4ae3;
        border-radius: 25px;
        height: 100%;
        box-sizing: border-box;
    }
    
    .border-logo {
        position: relative;
        bottom: 60px;
        left: 530px;
        height: 60px;
        background: white;
        display: inline-block;
    }
    
    .border-logo-image {
        height: 60px;
    }
    
    .contact-information {
        text-align: center;
    }
    
    .contact-information * {
        margin-bottom: 1px;
    }
    
    .contact-list {
        border-bottom: 1px solid;
    }
    
    .contact-list li {
        display: inline;
        margin-left: 5px;
        font-weight: 400;
        color: gray;
        font-size: x-small;
    }
    
    .title * {
        margin-bottom: 1px;
    }
    
    .contact-list li:not(:last-child) {
        border-right: dotted grey 1px;
    }
    
    .description {
        float: left;
    }
    
    .property {
        float: right;
    }
    
    .information-block {
        margin-top: 5px;
    }
    
    .right-information .information-block:first-of-type {
        margin-top: 0;
    }
    
    .last-updated {
        position: relative;
        bottom: 90px;
        left: 25px;
    }
    
    .history {
        
    }
    
</style>

<body>
<div class="page">
    <div class="page-content">
    <div class="outer-border">
        <div class="inner-border">
            <div class="row contact-information">
                <div class="col-12">
                    <h4>Fictional Tugboat, Co.</h4>
                    <ul class="contact-list">
                        <li>
                            742 Evergreen Terrace 
                        </li>
                        <li>
                            Springfield, USA
                        </li>
                        <li>
                            Phone: (939)-555-0113
                        </li>
                        <li>
                            Fax: (939)-555-0113
                        </li>
                        <li>
                            Email : chalmers@fictionaltugboatcompany.com
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row ">
                <div class="col-12 px-3 text-center">
                    <h1>{{name}}</h1>
                    <h6>{{caption}}</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-6 pl-4 pr-1">

                    <figure class="text-center title">
                        <img class=" img-fluid rounded" src="{{imageUrl}}" />
                        <figcaption class="history">{{history}}</figcaption>
                    </figure>
                    {{#each detailSections }}
                        {{#ifeq type "leftInformation"}}
                        <div class="information-block">

                            <b>{{name}}</b>
                            <div>
                                {{#each sectionDetails}}
                                <div>
                                    <span class="ml-1">
                                        {{description}}:
                                    </span>
                                    <span>
                                        {{#each values}}
                                        <div class="property">
                                            {{value}}
                                        </div>
                                        <br />
                                        {{/each}}
                                    </span>

                                </div>
                                {{/each}}
                            </div>

                        </div>
                        {{/ifeq}}
                    {{/each}}
                </div>
                
                <div class="right-information col-6 pr-4 pl-2">
                    {{#each detailSections }}
                    {{#ifeq type "rightInformation"}}
                    <div class="information-block">

                        <b>{{name}}</b>
                        <div>
                            {{#each sectionDetails}}
                            <div>
                                <span class="ml-1">
                                    {{description}}:
                                </span>
                                <span>
                                    {{#each values}}
                                    <div class="property">
                                        {{value}}
                                    </div>
                                    <br />
                                    {{/each}}
                                </span>

                            </div>
                            {{/each}}
                        </div>

                    </div>
                    {{/ifeq}}
                    {{/each}}
                </div>
            </div>


        </div>
    <span class="border-logo">
        <img class="border-logo-image" src="/logo.png" />
    </span>
    <div class="last-updated">
        <small>Last Updated: {{updated}}</small>
    </div>
    </div>
    </div>
    </div>

</body>

</html>`
export default specSheetTemplate;