/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
    mount: {
        public: '/',
        src: '/dist'
    },
    plugins: [
        '@snowpack/plugin-svelte'
    ],
    packageOptions: {
        polyfillNode:true
    },
    devOptions: {
        /* ... */
    },
    buildOptions: {
        /* ... */
    },
    alias: {
        /* ... */
    },
    optimize: {
        bundle: true,
        minify: true,
        target: 'es2018'
    }
};