# Specsheet Generator API   
This project provides a REST api for persisting information for the specsheet generator project and provides a method to generate the actual PDFs. 

## Development 
This project relies on an actual database for development, because of that we use a docker-compose file so we can start up a Postgres SQL container to go along with the service. 

From this folder run
```
docker-compose up -d
```

From vscode use the "Remote Containers" extension to connect to the "app" container. 
From there use mvn spring-boot:run to run the project, it will be forwarded to your local machine on port 8080.