package com.jgore43.specsheetgeneratorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpecsheetGeneratorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpecsheetGeneratorApiApplication.class, args);
	}

}
