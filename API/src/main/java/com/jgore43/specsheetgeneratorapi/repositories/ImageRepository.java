package com.jgore43.specsheetgeneratorapi.repositories;

import com.jgore43.specsheetgeneratorapi.domain.Image;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, String> {
    
}
