package com.jgore43.specsheetgeneratorapi.repositories;

import com.jgore43.specsheetgeneratorapi.domain.Boat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BoatRespoitory extends JpaRepository<Boat, String> {

    
}
