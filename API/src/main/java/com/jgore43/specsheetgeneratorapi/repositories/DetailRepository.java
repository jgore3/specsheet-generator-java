package com.jgore43.specsheetgeneratorapi.repositories;

import java.util.List;
import java.util.Optional;

import com.jgore43.specsheetgeneratorapi.domain.Detail;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailRepository  extends JpaRepository<Detail,String> {
    Optional<List<Detail>> findByDetailSectionId(String detailSectionId);
}
