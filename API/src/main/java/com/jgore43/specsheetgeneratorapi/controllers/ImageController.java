package com.jgore43.specsheetgeneratorapi.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import com.jgore43.specsheetgeneratorapi.domain.Image;
import com.jgore43.specsheetgeneratorapi.services.ImageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
public class ImageController {

    @Autowired
    ImageService imageSerice;

    @RequestMapping(value = "/images/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] getImage(@PathVariable String id) {
        return imageSerice.getImage(id);
    }

    @PostMapping("/images")
    public Image saveImage(@RequestPart(value = "file") MultipartFile file,
            @RequestPart(value = "file2", required = false) MultipartFile file2) {
        return imageSerice.saveImage(file);
    }

    @RequestMapping(value = "/logo", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] getLogo() throws IOException {
        Path fileLocation = ResourceUtils.getFile("classpath:static/logo.png").toPath();
        return Files.readAllBytes(fileLocation);
    }

}
