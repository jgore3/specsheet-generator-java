package com.jgore43.specsheetgeneratorapi.controllers;

import java.util.List;

import com.jgore43.specsheetgeneratorapi.domain.Detail;
import com.jgore43.specsheetgeneratorapi.services.DetailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DetailController {

    @Autowired
    private DetailService detailService;

    @GetMapping("/boats/{boatId}/detail-section/{detailSectionId}/details")
    public List<Detail> getDetails (@PathVariable String detailSectionId){
        return detailService.getDetails(detailSectionId);
        
    }
    @GetMapping("/boats/{boatId}/detail-section/{detailSectionId}/details/{id}")
    public Detail getSpecificDetail (@PathVariable String id){
        return detailService.getDetail(id);
    }

    @PostMapping("/boats/{boatId}/detail-section/{detailSectionId}/details")
    public Detail saveDetail(@PathVariable String detailSectionId, @RequestBody Detail theDetail){
        return detailService.saveDetail(detailSectionId, theDetail);

    }

    @DeleteMapping("/boats/{boatId}/detail-section/{detailSectionId}/details/{id}")
    public void deleteDetail(@PathVariable String id){
        detailService.deleteDetail(id);
    }
}
