package com.jgore43.specsheetgeneratorapi.controllers;

import java.util.List;
import javax.servlet.http.HttpServletResponse;


import com.jgore43.specsheetgeneratorapi.domain.Boat;
import com.jgore43.specsheetgeneratorapi.services.BoatService;
import com.jgore43.specsheetgeneratorapi.services.PdfService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
public class BoatController {

    @Autowired
    private BoatService boatService;

    @Autowired
    private PdfService pdfService;
    @GetMapping("/boats")
    public List<Boat> getBoats(){
        return boatService.getAllBoats();
        
    }
    @GetMapping("/boats/{id}")
    public Boat getSpecificBoat (@PathVariable String id){
        return boatService.getSpecificBoat(id);

    }

    @PutMapping("/boats/{id}")
    public Boat updateSpecificBoat(@PathVariable String id, @RequestBody Boat theBoat){
        return boatService.updateSpecificBoat(id, theBoat);
    }

    @PostMapping("/boats")
    public Boat saveBoat(@RequestBody Boat theBoat){
        return boatService.saveBoat(theBoat);

    }

    @DeleteMapping("/boats/{id}")
    public void deleteBoat(@PathVariable String id){
     boatService.deleteBoat(id);
    }

    @GetMapping(value="/boats/{id}/pdf",produces= MediaType.APPLICATION_PDF_VALUE )
    public byte[] getPdf(@PathVariable String id, HttpServletResponse response){
        return pdfService.getPdf(id, response);
    }
}
