package com.jgore43.specsheetgeneratorapi.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletResponse;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.itextpdf.html2pdf.HtmlConverter;
import com.jgore43.specsheetgeneratorapi.domain.Boat;
import com.jgore43.specsheetgeneratorapi.repositories.BoatRespoitory;
import com.jgore43.specsheetgeneratorapi.repositories.ImageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;


@Service
public class PdfService {
    @Autowired
    BoatRespoitory boatRepo;

    @Autowired
    ImageRepository imageRepo;

    public byte[] getPdf(String boatId, HttpServletResponse response) {

        Boat theBoat = boatRepo.findById(boatId).orElseThrow();
        response.setHeader("Content-Disposition", "attachment; filename="+theBoat.getName()+" Specsheet.pdf"); 
        Handlebars handlebars = new Handlebars();
        try {
            handlebars.registerHelpers(ResourceUtils.getFile("classpath:templates/helpers.js"));
            File file = ResourceUtils.getFile("classpath:templates/specsheetTemplate.html");
            String content = new String(Files.readAllBytes(file.toPath()));
            Template template = handlebars.compileInline(content);
            String templateString = template.apply(theBoat);
            return generatePdfFromHtml(templateString);
        } catch ( Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] generatePdfFromHtml(String html) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            HtmlConverter.convertToPdf(html, outputStream);
            byte[] theBytes = outputStream.toByteArray();
            outputStream.close();
            return theBytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
