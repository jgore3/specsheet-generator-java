package com.jgore43.specsheetgeneratorapi.services;

import com.jgore43.specsheetgeneratorapi.domain.Image;
import com.jgore43.specsheetgeneratorapi.repositories.ImageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {

    @Autowired
    ImageRepository imageRepo;


    public byte[] getImage (String imageId){
        Image theImage = imageRepo.findById(imageId).orElseThrow();
        return theImage.getImage();
    }

    public Image saveImage(MultipartFile image){
        try{
        Image newImage = new Image();
        newImage.setImage(image.getBytes());
        
        return imageRepo.save(newImage);
        }catch (Exception e){
            return null;
        }

    }
    
}
