package com.jgore43.specsheetgeneratorapi.services;

import java.util.List;

import com.jgore43.specsheetgeneratorapi.domain.Boat;
import com.jgore43.specsheetgeneratorapi.repositories.BoatRespoitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoatService {

    @Autowired
    private BoatRespoitory boatRepo;

    public List<Boat> getAllBoats(){

        return (List<Boat>) boatRepo.findAll();

    }

    public Boat saveBoat(Boat theBoat){
       return boatRepo.save(theBoat);

    }

    public void deleteBoat(String boatId) {
        Boat theBoat = boatRepo.findById(boatId).orElseThrow();
         boatRepo.delete(theBoat);
    }

    public Boat getSpecificBoat(String id) {
       return boatRepo.findById(id).orElseThrow();
    }

    public Boat updateSpecificBoat(String id, Boat theBoat) {
       boatRepo.findById(id).orElseThrow();
       return boatRepo.save(theBoat);
    }
    
}
