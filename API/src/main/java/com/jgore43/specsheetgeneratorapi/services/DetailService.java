package com.jgore43.specsheetgeneratorapi.services;

import java.util.List;

import com.jgore43.specsheetgeneratorapi.domain.Detail;
import com.jgore43.specsheetgeneratorapi.repositories.DetailRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailService {
    @Autowired
    private DetailRepository detailRepo;

    public Detail getDetail(String detailId){
        return detailRepo.findById(detailId).orElseThrow();
    }
    public List<Detail> getDetails (String detailSectionId){
        return detailRepo.findByDetailSectionId(detailSectionId).orElseThrow();
    }   
    public Detail saveDetail(String detailSectionId, Detail detail){
        detail.setDetailSectionId(detailSectionId);
        return detailRepo.save(detail);
    }
    public void deleteDetail (String detailId){
        detailRepo.deleteById(detailId);
    }
}
