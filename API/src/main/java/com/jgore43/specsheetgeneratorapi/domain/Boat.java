package com.jgore43.specsheetgeneratorapi.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="boat")
@Getter
@Setter
public class Boat {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String caption;
    private String history;
    private String imageUrl;
    private String imageId;
    private String name;
    @CreationTimestamp
    private Date lastUpdated;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="boatId")
    private List<DetailSection> detailSections;
   
    public String getLastUpdatedString(){
        return new SimpleDateFormat("MM/dd/yyyy").format(lastUpdated);
    }
}
