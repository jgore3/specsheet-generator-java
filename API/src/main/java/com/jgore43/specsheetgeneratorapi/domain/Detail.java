package com.jgore43.specsheetgeneratorapi.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
@Entity
@Table(name="detail")
public class Detail {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String Id;
    private String description; 
    private String detailSectionId;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="detailId")
    private List<Value> values;

}
